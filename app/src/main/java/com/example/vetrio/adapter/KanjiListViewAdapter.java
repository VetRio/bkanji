package com.example.vetrio.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vetrio.bkanji.R;
import com.example.vetrio.converter.ConvertString;

public class KanjiListViewAdapter extends CursorAdapter {
    public KanjiListViewAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.items_listview_kanji, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView kanji_word_tv = (TextView) view.findViewById(R.id.kanji_word_tv);
        TextView kanji_hanviet_tv = (TextView) view.findViewById(R.id.kanji_hanviet_tv);
        TextView kanji_kunread_tv = (TextView) view.findViewById(R.id.kanji_kunread_tv);

        // Extract properties from cursor
        kanji_word_tv.setText(cursor.getString(1));
        kanji_hanviet_tv.setText("" + cursor.getString(2));
        kanji_kunread_tv.setText("Nghĩa:\t" + new ConvertString().getMeanKanji(cursor.getString(6)));
    }
}