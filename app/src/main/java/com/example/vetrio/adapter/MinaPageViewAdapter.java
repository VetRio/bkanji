package com.example.vetrio.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.vetrio.fragment.MinaLessonFragment;
import com.example.vetrio.fragment.MinaVocabFragment;

/**
 * Created by nguye on 9/21/2015.
 */
public class MinaPageViewAdapter extends FragmentStatePagerAdapter {

    private String data1, data2;
    private MinaVocabListViewAdapter adapter;
    private MinaVocabFragment mMinaVocabFragment;
    private MinaLessonFragment mMinaLessonFragment1, mMinaLessonFragment2;

    public MinaPageViewAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                mMinaLessonFragment1 = new MinaLessonFragment();
                if(data1 != null)
                    mMinaLessonFragment1.setData(data1);
                return mMinaLessonFragment1;
            case 1:
                mMinaLessonFragment2 = new MinaLessonFragment();
                if(data2 != null)
                    mMinaLessonFragment2.setData(data2);
                return mMinaLessonFragment2;
            case 2:
                mMinaVocabFragment = new MinaVocabFragment();
                if(adapter != null)
                    mMinaVocabFragment.setAdapter(adapter);
                return mMinaVocabFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 3;
    }

    public void setAdapter(MinaVocabListViewAdapter adapter){
        this.adapter = adapter;
    }
    public void setData1(String data1){
        this.data1 = data1;
    }
    public void setData2(String data2){
        this.data2 = data2;
    }
}
