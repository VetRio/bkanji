package com.example.vetrio.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vetrio.bkanji.DevelopersActivity;
import com.example.vetrio.bkanji.DictionActivity;
import com.example.vetrio.bkanji.FingerDrawActivity;
import com.example.vetrio.bkanji.GameActivity;
import com.example.vetrio.bkanji.JlptActivity;
import com.example.vetrio.bkanji.KanjiActivity;
import com.example.vetrio.bkanji.MinaActivity;
import com.example.vetrio.bkanji.R;
import com.example.vetrio.bkanji.VoiceActivity;

/**
 * Created by VetRio on 8/29/2015.
 */
public class MainListViewAdapter extends ArrayAdapter<String> {

    private Context context;
    private int count;
    private int drawableColorID[];
    private int drawableIconID[];
    private String drawableTite[];
    private Intent intents[];

    public MainListViewAdapter(Context context) {
        super(context, 0);
        this.context = context;
        count = 5;
        drawableColorID = new int[]{
                R.drawable.c1_bg,
                R.drawable.c2_bg,
                R.drawable.c3_bg,
                R.drawable.c4_bg,
                R.drawable.c5_bg,
                R.drawable.c6_bg,
                R.drawable.c7_bg,
                R.drawable.c8_bg,
                R.drawable.c9_bg,
                R.drawable.c10_bg};

        drawableIconID = new int[]{
                R.drawable.ic_search,
                R.drawable.ic_game,
                R.drawable.ic_book,
                R.drawable.ic_brush,
                R.drawable.ic_test,
                R.drawable.ic_voice_recognition,
                R.drawable.ic_exam,
                R.drawable.ic_setting,
                R.drawable.ic_contact,
                R.drawable.ic_shutdown
        };

        drawableTite = new String[]{
                "Tìm kiếm kanji",
                "Trò chơi",
                "Tra việt-nhật",
                "Tìm nét vẽ",
                "Học theo bài",
                "Phiên dịch viên",
                "Kiểm tra trình độ",
                "Cài đặt chung",
                "Nhà phát triển",
                "Tắt ứng dụng"
        };

        intents = new Intent[]{
                new Intent(context, KanjiActivity.class),
                new Intent(context, GameActivity.class),
                new Intent(context, DictionActivity.class),
                new Intent(context, FingerDrawActivity.class),
                new Intent(context, MinaActivity.class),
                new Intent(context, VoiceActivity.class),
                new Intent(context, JlptActivity.class),
                null,
                new Intent(context, DevelopersActivity.class)
        };
    }

    @Override
    public int getCount(){
        return count;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        if(convertView == null){
            convertView = inflater.inflate(R.layout.items_listview_main_menu, null);
        }

        ImageView icon_iv1 = (ImageView) convertView.findViewById(R.id.imageview1);
        TextView title_tv1 = (TextView) convertView.findViewById(R.id.textview1);
        ImageView icon_iv2 = (ImageView) convertView.findViewById(R.id.imageview2);
        TextView title_tv2 = (TextView) convertView.findViewById(R.id.textview2);

        icon_iv1.setBackgroundResource(drawableColorID[position*2]);
        icon_iv1.setImageResource(drawableIconID[position * 2]);
        title_tv1.setText(drawableTite[position * 2]);
        icon_iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(intents[position * 2]);
            }
        });

        icon_iv2.setBackgroundResource(drawableColorID[position * 2 + 1]);
        icon_iv2.setImageResource(drawableIconID[position * 2 + 1]);
        title_tv2.setText(drawableTite[position * 2 + 1]);
        icon_iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((position*2 + 1) == 9) ((Activity)context).finish();
                else if((position*2 + 1) != 7) context.startActivity(intents[position*2+1]);
            }
        });
        return convertView;
    }
}