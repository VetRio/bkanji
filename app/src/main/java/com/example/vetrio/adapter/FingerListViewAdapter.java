package com.example.vetrio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.vetrio.bkanji.R;
import com.example.vetrio.fingeritem.FingerSearchItem;
import com.example.vetrio.fingeritem.PercentView;

import java.text.DecimalFormat;

public class FingerListViewAdapter extends ArrayAdapter<FingerSearchItem> {
    public Context context;
    FingerSearchItem[] fingerSearchItems;

    public FingerListViewAdapter(Context context, FingerSearchItem[] fingerSearchItems) {
        super(context, 0, fingerSearchItems);
        this.context = context;
        this.fingerSearchItems = fingerSearchItems;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.items_listview_finger_commit, null);
        }

        TextView kanji_word_tv = (TextView) convertView.findViewById(R.id.kanji_word_tv);
        kanji_word_tv.setText(fingerSearchItems[position].getKanji());

        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        TextView scoreTextView = (TextView) convertView.findViewById(R.id.score_tv);
        scoreTextView.setText("Score: \n" + decimalFormat.format(fingerSearchItems[position].getScore()) + "%");

        TextView hanvietTextView = (TextView) convertView.findViewById(R.id.kanji_hanviet_tv);
        if (fingerSearchItems[position].getData() != null)
        {
            String[] data = fingerSearchItems[position].getData();
            hanvietTextView.setText(data[1]);
        }

        PercentView percentView = (PercentView) convertView.findViewById(R.id.percentview);
        percentView.setPercentage(fingerSearchItems[position].getScore());

        return convertView;
    }
}