package com.example.vetrio.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vetrio.bkanji.R;

public class MinaListViewAdapter  extends CursorAdapter {
    public MinaListViewAdapter(Context context, Cursor cursor) {
        super(context, cursor, true);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.items_listview_mina, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView name = (TextView) view.findViewById(R.id.name);
        TextView lesson = (TextView) view.findViewById(R.id.lesson);
        lesson.setText("第" + (cursor.getPosition() + 1) + "課");
        name.setText(cursor.getString(1));
    }
}