package com.example.vetrio.canvasview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.example.vetrio.fingeritem.FingerSearchItem;
import com.example.vetrio.kanjirecognize.InputStroke;
import com.example.vetrio.kanjirecognize.KanjiInfo;
import com.example.vetrio.kanjirecognize.KanjiList;
import com.example.vetrio.kanjirecognize.KanjiMatch;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by nguye on 9/26/2015.
 */
public class CanvasView extends View {

    public int width;
    public int height;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Stack<Path> pathStack;
    Context context;
    private Paint mPaint;
    private float mX, mY;
    private static final float TOLERANCE = 5;
    private float startX, startY, lastX, lastY;
    private ArrayList<InputStroke> strokeArrayList;

    private FingerSearchItem[] fingerSearchItems;
    private KanjiMatch[] kanjiMatches;
    private KanjiInfo potentialKanji;

    private KanjiList kanjiList = null;

    InputStream databaseStream;

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        // we set a new Path
        mPath = new Path();
        pathStack = new Stack<Path>();

        strokeArrayList = new ArrayList<InputStroke>();
        try {
            databaseStream = context.getAssets().open("strokes-20100823.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // and we set a new Paint with the desired attributes
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(4f);


        try {
            kanjiList = new KanjiList(databaseStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // your Canvas will draw onto the defined Bitmap
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the mPath with the mPaint on the canvas when onDraw
        for (Path p : pathStack)
            canvas.drawPath(p, mPaint);
    }

    // when ACTION_DOWN start touch according to the x,y values
    private void startTouch(float x, float y) {
        mPath = new Path();
        pathStack.push(mPath);
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    // when ACTION_MOVE move touch according to the x,y values
    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    public void clearCanvas() {
        pathStack.clear();
        strokeArrayList.clear();
        invalidate();
    }

    public void undoCanvas() {
        if (!pathStack.empty()) {
            pathStack.pop();
            if (!strokeArrayList.isEmpty()) strokeArrayList.remove(strokeArrayList.size() - 1);
        }
        invalidate();
    }

    public FingerSearchItem[] updateFromChangeCanvas() throws IOException {
        InputStroke[] inputStrokes = new InputStroke[strokeArrayList.size()];
        Log.d("Stroke", inputStrokes.length + "");
        for (int i = 0; i < strokeArrayList.size(); i++) {
            inputStrokes[i] = strokeArrayList.get(i);
        }
        onRecognize(inputStrokes);

        fingerSearchItems = new FingerSearchItem[kanjiMatches.length];
        for (int i = 0; i < kanjiMatches.length; i++) {
            fingerSearchItems[i] = new FingerSearchItem(kanjiMatches[i].getKanji().getKanji(), kanjiMatches[i].getScore());
        }
        invalidate();
        return fingerSearchItems;
    }

    // when ACTION_UP stop touch
    private void upTouch() {
        mPath.lineTo(mX, mY);
    }

    //override the onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                setStartCoordinate(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                setEndCoordinate(x, y);
                InputStroke stroke = new InputStroke(startX, startY, lastX, lastY);
                strokeArrayList.add(stroke);
                invalidate();
                break;
        }
        return true;
    }

    public void setStartCoordinate(float startX, float startY) {
        this.startX = startX;
        this.startY = startY;
    }

    public void setEndCoordinate(float endX, float endY) {
        this.lastX = endX;
        this.lastY = endY;
    }


    public void onRecognize(InputStroke[] strokes) throws IOException {

        potentialKanji = new KanjiInfo("?");
        for (InputStroke stroke : strokes) {
            potentialKanji.addStroke(stroke);
        }
        potentialKanji.finish();

        kanjiMatches = kanjiList.getTopMatches(potentialKanji, KanjiInfo.MatchAlgorithm.STRICT, null);
    }

}