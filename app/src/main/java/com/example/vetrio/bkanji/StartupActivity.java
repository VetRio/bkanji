package com.example.vetrio.bkanji;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.vetrio.database.DataBaseHeplper;

import java.io.IOException;


public class StartupActivity extends AppCompatActivity {

    private ImageView logo_iw;
    private static final String[] DB_NAME = {"JLPTTest.db","minnaJP.db", "bkanji.db", "vietnamese_japanese.db", "japanese_vietnamese.db"};
    private static final int[] TOTAL_DATA = {533, 1216, 3338, 14307, 19966};
    Context context = this;
    private int progressStatus = 0;
    private ProgressDialog progressDialog;
    private boolean isExits = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        logo_iw = (ImageView) findViewById(R.id.logo_iw);

        for (int i = 0; i < DB_NAME.length; i++) {
            if(!checkDataBase(DB_NAME[i], TOTAL_DATA[i])){
                isExits = false;
                break;
            }
        }
        if(!isExits){
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Đang copy dữ liệu...\n(Chỉ một lần duy nhất)");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgress(0);
            int max = 0;
            for (int i = 0; i < DB_NAME.length; i++)
                max += TOTAL_DATA[i];
            progressDialog.setMax(max);

            MyAsyncTask asyncTask = new MyAsyncTask();
            asyncTask.execute();
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(StartupActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 1000);
        }
    }

    public boolean checkDataBase(String dbName, int totalData) {
        DataBaseHeplper dataBaseHeplper = new DataBaseHeplper(this, dbName, totalData);
        return dataBaseHeplper.checkDataBase();
    }

    public void createDataBase(String dbName, int totalData) {
        DataBaseHeplper dataBaseHeplper = new DataBaseHeplper(this, dbName, totalData);
        try {
            dataBaseHeplper.createDataBase();
            dataBaseHeplper.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        progressStatus += dataBaseHeplper.getByteCopyed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public class MyAsyncTask extends AsyncTask<Void, Integer, Void> {

        //hàm này sẽ được thực hiện đầu tiên
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog.show();
        }

        //sau đó tới hàm doInBackground
        //tuyệt đối không được cập nhật giao diện trong hàm này
        @Override
        protected Void doInBackground(Void... arg0) {

            for (int i = 0; i < DB_NAME.length; i++) {
                createDataBase(DB_NAME[i], TOTAL_DATA[i]);
                //khi gọi hàm này thì onProgressUpdate sẽ thực thi
                publishProgress();
            }
            return null;
        }
        /**
         * ta cập nhập giao diện trong hàm này
         */
        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
            progressDialog.setProgress(progressStatus);
        }
        /**
         * sau khi tiến trình thực hiện xong thì hàm này sảy ra
         */
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            Intent intent = new Intent(StartupActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
