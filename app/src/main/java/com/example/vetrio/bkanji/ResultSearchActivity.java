package com.example.vetrio.bkanji;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import java.util.Locale;


public class ResultSearchActivity extends AppCompatActivity {

    private WebView contentWebView;
    private Toolbar toolbar;
    private String dataHtml;
    private String newWord;
    private TextToSpeech textToSpeech;
    private Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_search);
        Intent intent = getIntent();
        dataHtml = intent.getStringExtra("dataHtml");
        newWord = intent.getStringExtra("newWord");

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("Kết quả tra từ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = textToSpeech.setLanguage(Locale.JAPANESE);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                        Toast.makeText(ctx, "This text to speech not supported", Toast.LENGTH_SHORT).show();;
                        /*Intent installIntent = new Intent();
                        installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                        startActivity(installIntent);*/
                    }
                }
            }
        });


        contentWebView = (WebView) findViewById(R.id.content_wv);
        contentWebView.loadDataWithBaseURL(null,dataHtml, "text/html", "utf-8",null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.action_head:
                openHeading();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openHeading(){
        textToSpeech.speak(newWord, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void onPause(){
        if(textToSpeech !=null){
            textToSpeech.stop();
        }
        super.onPause();
    }

    public void onDestroy(){
        if(textToSpeech !=null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }
}
