package com.example.vetrio.bkanji;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class JlptActivity extends AppCompatActivity {

    private Button[] n;
    private Button[] u;
    private Button start;
    private int code = -1;
    private int nmay = -1;
    private Context ctx = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jlpt);

        n = new Button[5];

        n[0] = (Button) findViewById(R.id.n1);
        n[1] = (Button) findViewById(R.id.n2);
        n[2] = (Button) findViewById(R.id.n3);
        n[3] = (Button) findViewById(R.id.n4);
        n[4] = (Button) findViewById(R.id.n5);

        u = new Button[3];
        u[0] = (Button) findViewById(R.id.hantu);
        u[1] = (Button) findViewById(R.id.tuvung);
        u[2] = (Button) findViewById(R.id.nguphap);

        start = (Button) findViewById(R.id.start);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(code < 0) Toast.makeText(ctx, "Bạn chưa chọn loại kiểm tra", Toast.LENGTH_SHORT).show();
                else if(nmay < 0) Toast.makeText(ctx, "Bạn chưa chọn mức độ kiểm tra", Toast.LENGTH_SHORT).show();
                else{
                    Intent intent = new Intent(ctx, JlptTestActivity.class);
                    intent.putExtra("n", nmay);
                    intent.putExtra("code", code);
                    startActivity(intent);
                }

            }
        });

        for (int i = 0; i < 3; i++){
            final int finalI = i;
            u[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int j = 0; j < 3; j++){
                        u[j].setBackgroundResource(R.drawable.bg_help_blue);
                    }
                    u[finalI].setBackgroundResource(R.drawable.bg_help_red);
                    code = finalI + 1;
                }
            });
        }
        for (int i = 0; i < 5; i++){
            final int finalI = i;
            n[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int j = 0; j < 5; j++){
                        n[j].setBackgroundResource(R.drawable.bg_help_blue);
                    }
                    n[finalI].setBackgroundResource(R.drawable.bg_help_red);
                    nmay = finalI;
                }
            });
        }

    }
}