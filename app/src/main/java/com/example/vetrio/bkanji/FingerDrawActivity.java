package com.example.vetrio.bkanji;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vetrio.adapter.FingerListViewAdapter;
import com.example.vetrio.canvasview.CanvasView;
import com.example.vetrio.converter.ConvertString;
import com.example.vetrio.database.DataBaseHeplper;
import com.example.vetrio.fingeritem.FingerSearchItem;

import java.io.IOException;

public class FingerDrawActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public CanvasView drawView;
    public Button undoButton;
    public Button clearButton;
    public Button searchButton;
    public ProgressBar progressBar;
    private Dialog dialog;
    private ListView listView;

    private static final String DB_NAME = "bkanji.db";
    private static final String TABLE_NAME = "kanjidata";
    private static final String COLUMN_NAME = "kanji";


    public DataBaseHeplper dataBaseHeplper;
    public Cursor cursor;
    private boolean canBack = true;

    private FingerSearchItem[] fingerSearchItems;
    public Context context = this;

    private FingerListViewAdapter fingerArrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_draw);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawView = (CanvasView) findViewById(R.id.draw_view);
        undoButton = (Button) findViewById(R.id.undo_bt);
        clearButton = (Button) findViewById(R.id.clear_bt);
        searchButton = (Button) findViewById(R.id.search_bt);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressBar.setVisibility(View.GONE);

        toolbar.setTitle("Tìm nét vẽ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //CreateDatabase
        dataBaseHeplper = new DataBaseHeplper(this, DB_NAME);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.clearCanvas();
            }
        });

        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawView.undoCanvas();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                canBack = false;
                try {
                    fingerSearchItems = drawView.updateFromChangeCanvas();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                getData();
                showDialog(fingerSearchItems);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    private void showDialog(final FingerSearchItem[] fingerSearchItems) {

        dialog = new Dialog(this);
        listView = new ListView(context);
        fingerArrayAdapter = new FingerListViewAdapter(this, fingerSearchItems);
        listView.setAdapter(fingerArrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(fingerSearchItems[position].getData() != null) {
                    String[] data = fingerSearchItems[position].getData();
                    Intent intent = new Intent(FingerDrawActivity.this, ResultSearchActivity.class);
                    intent.putExtra("dataHtml", new ConvertString().getHtmlKanji(data));
                    intent.putExtra("newWord",data[0]);
                    startActivity(intent);
                }
            }
        });

        dialog.setContentView(listView);
        dialog.setTitle("Kết quả tìm kiếm");
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (canBack) {
            super.onBackPressed();

            if(cursor != null) cursor.close();
            if(dataBaseHeplper != null) dataBaseHeplper.close();
            if(dialog != null && dialog.isShowing())
                dialog.dismiss();
        } else
            Toast.makeText(this, "Đang lấy dữ liệu, vui lòng chờ!", Toast.LENGTH_SHORT).show();
    }

    public void getData() {

        progressBar.setVisibility(View.VISIBLE);
        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                for(int i = 0; i < fingerSearchItems.length; i++){
                    Cursor cursor = dataBaseHeplper.getInformation(TABLE_NAME, COLUMN_NAME, fingerSearchItems[i].getKanji());
                    if(cursor.getCount() != 0)
                    {
                        cursor.moveToFirst();
                        String data[] = new String[7];

                        for (int j= 0; j < 7; j++){
                            data[j] = cursor.getString(j+1);
                        }
                        fingerSearchItems[i].setData(data);
                    }
                }
                return null;
            }

            protected void onProgressUpdate(Integer... values) {
            }
            protected void onPostExecute(Void result) {
                fingerArrayAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                canBack = true;
            }
        };
        asyncTask.execute();
    }
}
