package com.example.vetrio.bkanji;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

import java.util.ArrayList;
import java.util.Locale;

public class VoiceActivity extends Activity {

	private TextView txtSpeechInput;
	private ImageView btnSpeak;
	private ImageView btnSetting, btnHead, btnTranslate;
	private final int REQ_CODE_SPEECH_INPUT = 100;
	private AlertDialog levelDialog;
	private boolean isTiengVietInput = false;
	private boolean error = false;
	private TextToSpeech textToSpeech;
	private Context ctx = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_voice);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
		btnSpeak = (ImageView) findViewById(R.id.btnSpeak);
		btnHead = (ImageView) findViewById(R.id.btnHead);
		btnSetting= (ImageView) findViewById(R.id.btnSetting);
		btnTranslate= (ImageView) findViewById(R.id.btnTranslate);

		btnSpeak.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				promptSpeechInput();
			}
		});

		btnSetting.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				promptLanguageInput();
			}
		});

		btnTranslate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!txtSpeechInput.getText().toString().equals("")) {
					try {
						translateShow(txtSpeechInput.getText().toString());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		btnHead.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!txtSpeechInput.getText().toString().equals("") && isTiengVietInput) {
					try {
						translateShow(txtSpeechInput.getText().toString());
						openSpeech(translate(txtSpeechInput.getText().toString()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				if (status == TextToSpeech.SUCCESS) {
					int result = textToSpeech.setLanguage(Locale.JAPANESE);
					if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
						Toast.makeText(ctx, "This text to speech not supported", Toast.LENGTH_SHORT).show();;
						Log.e("TTS", "This Language is not supported");
						/*Intent installIntent = new Intent();
						installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
						startActivity(installIntent);*/
					}
				}
			}
		});
	}

	/**
	 * Showing google speech input dialog
	 * */
	private void promptSpeechInput() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		if(isTiengVietInput == false)
			intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ja");
		else intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi");
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
				getString(R.string.speech_prompt));
		try {
			startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
		} catch (ActivityNotFoundException a) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.speech_not_supported),
					Toast.LENGTH_SHORT).show();
		}
	}
	/**
	 * Showing language choose dialog
	 * */
	private void promptLanguageInput() {
		final CharSequence[] items = {"Tiếng việt","日本語"};

		// Creating and Building the Dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Chọn ngôn ngữ");
		builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
					case 0:
						isTiengVietInput = true;
						break;
					default:
						isTiengVietInput = false;
						break;
				}
			}
		});
		levelDialog = builder.create();
		levelDialog.show();
	}
	/**
	 * Showing translate dialog
	 * */
	private void translateShow(String message) throws Exception {
		// Creating and Building the Dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Dịch nghĩa");
		builder.setMessage(translate(message));
		if(error) return;
		levelDialog = builder.create();
		levelDialog.show();
	}
	/**
	 * Translate using Bing
	 * */
	public String translate(String text) {
		// Set the Client ID / Client Secret once per JVM. It is set statically and applies to all
		System.out.print(text);
		Translate.setClientId("VetRio");
		Translate.setClientSecret("vetrio2511datamarket");

		String translatedText = "";
		try {
			if(isTiengVietInput == false)
				translatedText = Translate.execute(text, Language.JAPANESE, Language.VIETNAMESE);
			else translatedText = Translate.execute(text, Language.VIETNAMESE, Language.JAPANESE);
		} catch (Exception e) {
			e.printStackTrace();
			error = true;
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Có lỗi..");
			builder.setMessage("Xin vui lòng kiểm tra lại kết nối mạng!..");
			levelDialog = builder.create();
			levelDialog.show();
		}

		System.out.print(translatedText);
		return translatedText;
	}

	/**
	 * Speech output
	 * */
	public void openSpeech(String text) {
		textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
	}
	/**
	 * Receiving speech input
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
			case REQ_CODE_SPEECH_INPUT: {
				if (resultCode == RESULT_OK && null != data) {

					ArrayList<String> result = data
							.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					txtSpeechInput.setText(result.get(0));
				}
				break;
			}

		}
	}
	public void onPause(){
		if(textToSpeech !=null){
			textToSpeech.stop();
		}
		super.onPause();
	}

	public void onDestroy(){
		if(textToSpeech !=null){
			textToSpeech.stop();
			textToSpeech.shutdown();
		}
		super.onDestroy();
	}

}
