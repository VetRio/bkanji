package com.example.vetrio.bkanji;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.vetrio.adapter.MinaPageViewAdapter;
import com.example.vetrio.adapter.MinaVocabListViewAdapter;
import com.example.vetrio.database.DataBaseHeplper;


public class MinaLessonActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TextView textView;
    private MinaPageViewAdapter pageViewAdapter;

    private static final String DB_NAME = "minnaJP.db";
    private static final String TABLE_NAME[] = {"nguphap", "luyennghe", "vocabularys"};


    public DataBaseHeplper dataBaseHeplper;
    public Context context = this;
    public Cursor cursor1, cursor2, cursor3;
    int lesson = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mina_lesson);

        Intent intent = getIntent();
        lesson = intent.getIntExtra("position", 1);

        viewPager = (ViewPager) findViewById(R.id.pager);
        textView = (TextView) findViewById(R.id.name);
        textView.setText("第"+lesson+"課");

        //CreateDatabase
        dataBaseHeplper = new DataBaseHeplper(this, DB_NAME);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        pageViewAdapter = new MinaPageViewAdapter(getSupportFragmentManager());
        getData(lesson);
        viewPager.setAdapter(pageViewAdapter);
    }


    public void getData(final int lesson) {

        cursor1 = dataBaseHeplper.getInformation(TABLE_NAME[0], "idlesson", lesson);
        cursor2 = dataBaseHeplper.getInformation(TABLE_NAME[1], "idlesson", lesson);
        cursor3 = dataBaseHeplper.getInformation(TABLE_NAME[2], "idlesson", lesson);
        if (cursor1.getCount() != 0) {
            cursor1.moveToFirst();
            pageViewAdapter.setData1(cursor1.getString(1));
        }
        if (cursor2.getCount() != 0) {
            cursor2.moveToFirst();
            pageViewAdapter.setData2(cursor2.getString(3));
        }
        if (cursor3.getCount() != 0) {
            cursor3.moveToFirst();
            pageViewAdapter.setAdapter(new MinaVocabListViewAdapter(context, cursor3));

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (cursor1 != null) cursor1.close();
        if (cursor3 != null) cursor2.close();
        if (cursor2 != null) cursor3.close();
        if (dataBaseHeplper != null) dataBaseHeplper.close();

    }
}
