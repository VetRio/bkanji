package com.example.vetrio.bkanji;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class GameActivity extends AppCompatActivity {

    public Button singBtn, levelBtn, lawBtn, newBtn;
    public Context context = this;
    private int level = 0;
    private AlertDialog levelDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        singBtn = (Button) findViewById(R.id.btnSing);
        levelBtn = (Button) findViewById(R.id.btnLevel);
        lawBtn = (Button) findViewById(R.id.btnLaw);
        newBtn = (Button) findViewById(R.id.btnNew);


        levelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptLevelInput();
            }
        });

        lawBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GameActivity.this, GameLawActivity.class);
                startActivity(intent);
            }
        });
        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GameActivity.this, GamePlayActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Showing level choose dialog
     * */
    private void promptLevelInput() {
        final CharSequence[] items = {"Dễ","Trung bình", "Khó"};

        // Creating and Building the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Chọn độ khó");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                level = item;
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
    }
}
