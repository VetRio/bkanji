package com.example.vetrio.bkanji;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vetrio.adapter.MinaListViewAdapter;
import com.example.vetrio.database.DataBaseHeplper;

public class MinaActivity extends AppCompatActivity {

    private ListView list;
    private Context context = this;
    private MinaListViewAdapter adapter;
    private ProgressBar progressBar;

    private Cursor cursor;
    public DataBaseHeplper dataBaseHeplper;

    private static final String DB_NAME = "minnaJP.db";
    private static final String TABLE_NAME = "lessons";
    private static final String COLUMN_NAME = "name";

    private boolean canBack = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mina);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);


        //CreateDatabase
        dataBaseHeplper = new DataBaseHeplper(this, DB_NAME);
        try {
            dataBaseHeplper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        AsyncTask<Void, Integer, Void> asyncTask = new AsyncTask<Void, Integer, Void>() {
            @Override
            protected void onPreExecute(){
            }
            @Override
            protected Void doInBackground(Void... voids) {
                canBack = false;
                cursor = dataBaseHeplper.getInformation(TABLE_NAME, COLUMN_NAME, "");
                cursor.moveToFirst();
                return null;
            }
            protected void onProgressUpdate(Integer... values){
            }
            protected void onPostExecute(Void result) {
                progressBar.setVisibility(View.GONE);
                canBack = true;
                adapter = new MinaListViewAdapter(context, cursor);
                list.setAdapter(adapter);
            }
        };
        asyncTask.execute();

        list = (ListView) findViewById(R.id.list);
        adapter = new MinaListViewAdapter(context, cursor);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MinaActivity.this, MinaLessonActivity.class);
                intent.putExtra("position", i+1);
                startActivity(intent);
            }
        });

    }
    @Override
    public void onBackPressed(){
        if(canBack) {
            super.onBackPressed();
        }
        else
            Toast.makeText(this, "Đang lấy dữ liệu, vui lòng chờ!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        dataBaseHeplper.close();
    }
}